import users from '/home/alex/Рабочий стол/eltex/vue-authentication-example-master/users.json'

const mocks = {
  'auth': { 'POST': { token: 'This-is-a-mocked-token' } },
  'user/me': { 'GET': { name: 'doggo', title: 'sir' } }
}

const apiCall = ({url, method, ...args}) => new Promise((resolve, reject) => {
  setTimeout(() => {
    try {
      if (method || 'GET' === 'POST') {
        try {
          var found = false
          users.users.forEach(element => {
            console.log(`User: '${element.username}'`)
            if (!found) {
              if (element.username == args.data.username) {
                if (element.password == args.data.password) {
                  found = true
                  localStorage.setItem('user-name', args.data.username)
                  resolve(mocks[url][method || 'GET'])
                }
              }
            }
          })
          console.log(`args.data '${args.data.username}'`)
          console.log(`args.data '${args.data.password}'`)
          console.log(`Is in file: '${found}'`)
          if (!found) {
            reject(new Error('Unknown user/password record'))
          }
        }
        catch (err) {
          reject(new Error(err))
        }
      }
      else {
        var user = mocks[url][method || 'GET']
        user.name = localStorage.getItem('user-name')
        resolve(user)
        console.log(`Mocked '${url}' - ${method || 'GET'}`)
        console.log('response: ', mocks[url][method || 'GET'])
      }
    } catch (err) {
      reject(new Error(err))
    }
  }, 1000)
})

export default apiCall
